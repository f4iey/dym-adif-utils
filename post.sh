#!/bin/bash

# Usage: post.sh <CALLSIGN> <PATH/URL>

# Load YAML into variables
while IFS=': ' read -r key value; do
    if [[ $key && $value && $key != "#" ]]; then
        declare "$key=${value//\"/}"
    fi
done < <(sed -n '/^[^#]/p' ./config.yml)

# Upload with curl
if [[ $2 == "http*" ]]; then
    # Using curl with http
    raw=$(curl -s "$2")
    echo $raw | curl -X POST -b $cookie -H "Content-Type: multipart/form-data" \
    -H "Referer: https://events.ham-yota.com/stations/$1/adif/upload" \
    -F "file=@-;filename=$filename;type=application/octet-stream" \
    https://events.ham-yota.com/api/${!1}/adif/upload
else
    curl -X POST -b $cookie -H "Content-Type: multipart/form-data" \
    -H "Referer: https://events.ham-yota.com/stations/$1/adif/upload" \
    -F "file=@$2;filename=$filename;type=application/octet-stream" \
    https://events.ham-yota.com/api/${!1}/adif/upload
fi

# Make a request and capture the 'december_' cookie
response=$(curl -s -I "https://events.ham-yota.com/stations/$1/adif/upload")
new_cookie=$(echo "$response" | grep -iE '^Set-Cookie:' | grep -oE 'december[^;]+' | awk 'NR==1')

if [ -n "$new_cookie" ]; then
    cookie_field="cookie"
    # Update the YAML field with the new cookie value
    sed -i "s/^$cookie_field: .*/$cookie_field: \"$new_cookie\"/" ./config.yml
fi