import requests
import yaml
import sys

def upload_file(callsign, file_path_or_url, conf_data):
    # Extract values from the YAML data
    callsigns = conf_data.get('callsigns', {})
    filename = conf_data['filename']
    cookie = conf_data['cookie']
    upload_id = callsigns.get(callsign)

    if file_path_or_url.startswith("http"):
        # Fetch file content from URL
        response = requests.get(file_path_or_url)
        raw = response.text

        # Upload with fetched content
        url = f"https://events.ham-yota.com/api/{upload_id}/adif/upload"
        headers = {
            "Cookie": cookie,
            "Content-Type": "multipart/form-data",
            "Referer": f"https://events.ham-yota.com/stations/{callsign}/adif/upload"
        }
        files = {
            "file": (filename, raw, "application/octet-stream")
        }
        response = requests.post(url, headers=headers, files=files)
        print(response.text)
    else:
        # Upload file directly
        url = f"https://events.ham-yota.com/api/{upload_id}/adif/upload"
        headers = {
            "Cookie": cookie,
            "Content-Type": "multipart/form-data",
            "Referer": f"https://events.ham-yota.com/stations/{callsign}/adif/upload"
        }
        files = {
            "file": (filename, open(file_path_or_url, 'rb'), "application/octet-stream")
        }
        response = requests.post(url, headers=headers, files=files)
        print(response.text)
    
    # Store the new received cookie
    new_cookie = ''

    # Filter and capture the cookie starting with 'december_'
    if 'Set-Cookie' in response.headers:
        cookies = response.headers['Set-Cookie'].split('; ')
        for cookie in cookies:
            if cookie.startswith('december'):
                new_cookie = cookie
                break

    if new_cookie:
        conf_data['cookie'] = new_cookie

# Load YAML file
with open('config.yml', 'r') as file:
    conf_data = yaml.safe_load(file)

# Usage: python3 post.py <CALLSIGN> <PATH/URL>
# Check if callsign and file arguments were provided
if len(sys.argv) == 3:
    callsign = sys.argv[1]
    file_path = sys.argv[2]
    upload_file(callsign, file_path, conf_data)
else:
    print("Usage: python3 post.py <CALLSIGN> <PATH/URL>")