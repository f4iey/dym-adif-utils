# December YOTA Month ADIF Utils
Here, you'll find a bunch of scripts to help you automate ADIF log uploads on [December YOTA Month](https://events.ham-yota.com) platform, as a YOTA special call user or organizer.

*Disclamer: this method is reverse-engineered and a workaround and is not related to the official page. Do not use this unless you know what you are doing!*

## How to use
### Prerequisites
To use this automation, you will have to find out a few things first:

0. A SEC for DYM (obviously)
1. Have a resgistered account on [events.ham-yota.com](https://events/ham-yota.com/participate/1)
2. Tick "Remember me" when first login
3. Once logged in, get your `december_yota_month_$<CURRENT_YEAR>_session` cookie, it should look like this:
```
december_yota_month_2023_session=eyJpdiI6IllUK0pydzNFdlJzeTJZQ2E0ZmNDeXc9PSIsInZhbHVlIjoiZ2svbncrRlIyNXBMOVZyNHBId3pLLzRxclFFbUVzczRTTldwVHA2NkRROEhhTGRPNG1tbUFvL2F4S1dTNUEzMlJyLzQ2T3VlaVdUVU5MOWhXbFlqa3pKaHhZTktKbmU0d253SldCMXBhaDM5NjIyMzRPV253YkRsNEFrOHRjWXUiLCJtYWMiOiI3OTZiZWI5YTBlMmMzZjRhMDcyZmQ3MjU0OTI4YTUzZjc0MzUxODhhZjY2OTk2MWZhZmRhZjVlZjJjMmM2MzQwIiwidGFnIjoiIn0%3D;
```
**WARNING: DO NOT COMMUNICATE THIS SESSION COOKIE TO ANYBODY. THIS COULD LEAD INTO SOMEONE HAVING FULL ACCESS OVER YOUR ACCOUNT**

4. Your callsign API ID, present in the upload url that looks something like: `https://events.ham-yota.com/api/$<ID>/adif/upload`

All these informations can be found in a POST request apprearing on your browser under the network tab in the Dev Tools (F12) while uploading a file manually.

### Dependendies
#### Python
```bash
pip install requests pyyaml
```
#### Bash
For the bash script, make sure to install these packages:
`curl`

### Implementation examples
There are 2 example scripts for you to choose, either shell or python. Both of them rely on the `config.yml` where you need to put the information mentioned in the prerequisites above.
```bash
git clone https://gitlab.com/f4iey/dym-adif-utils.git
cd dym-adif-utils
cp config.yml.sample config.yml
vim config.yml
```
From here, you can fill your desired callsigns in this structure:
```yaml
# Format CALLSIGN: <upload_id>
callsigns:
  TM23YOTA: 0
  TM4YOTA: 1
```
Every callsign should be followed by their own API ID mentioned in step 4.
The `cookie` field should contain your own session cookie. Make sure you ticked "Remember me" at first or it will instantly expire! Although, the maximum duration is about an hour. So you'll have to refresh it periodically.
#### Cron
You can use a user cron expression to run one of these scripts every 30min, for instance:
```bash
crontab -e
```
`*/30 * * * * /path/to/dym-adif-utils/post.sh TM23YOTA https://cloudlog.org/path/to/adif/log.adi`
#### Systemd service
You could add something like this in `.config/systemd/user/post-adif.service`
```systemd
[Unit]
Description=Run post.py every 30 minutes

[Service]
ExecStart=/usr/bin/python3 /path/to/post.py OS23YOTA /home/user/Documents/myLog.adif

[Timer]
OnUnitActiveSec=30min
Persistent=true

[Install]
WantedBy=default.target
```
enable and run it:
```bash
systemctl enable post-adif
systemctl start post-adif
```

To avoid sending too much duplicate logs, refreshing the filename regularly or auto-delete using these methods are highly recommended!
